module.exports = {
  types: [
    {value: 'feat', name: 'feat: 新特性'},
    {value: 'fix', name: 'fix: bug修复'},
    {value: 'docs', name: 'docs: 文档改动'},
    {
      value: 'style',
      name: 'style: 样式改动'
    },
    {
      value: 'refactor',
      name: 'refactor: 代码重构'
    },
    {
      value: 'perf',
      name: 'perf: 性能优化'
    },
    {value: 'test', name: 'test: 测试相关'},
    {
      value: 'chore',
      name: 'chore: 构建和库相关'
    },
    {value: 'revert', name: 'revert: 回退提交'},
    {value: 'WIP', name: 'WIP: 待提交..'}
  ],

  messages: {
    type: '选择提交类型:\n',
    scope: '情输入修改的范围(可选):\n',
    subject: '简要描述提交内容(必须):\n',
    body: '详情描述提交内容(可选). 使用 "|" 来换行:\n',
    breaking: '列出变更列表 (可选):\n',
    footer: '关闭的issue(可选). 例如: #31, #34:\n',
    confirmCommit: '确认使用以上信息提交？(y/n)'
  },

  allowCustomScopes: false,
  allowBreakingChanges: ['feat', 'fix'],
  subjectLimit: 100,
  skipEmptyScopes:true   //避免显示成这样 feat(custom):xxx，实际应该是feat:xxx
}
