import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import dts from 'vite-plugin-dts' //自动生成.dts文件
import Case from 'case'
import VueSetupExtend from 'vite-plugin-vue-setup-extend'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import ElementPlusPlugin from 'unplugin-element-plus/vite'
import generatePackageJson from './generate-package-json'
import rootPkg  from '../package.json'
import {cloneDeep} from "lodash-es"

//======================库的配置区域 start==================
const baseUrl = '/'
const outDir = 'outDir';
//顶部插入文本
const banner = `/*!
* ${rootPkg.name} ${rootPkg.version}
*
* @author ${rootPkg.author}
* @link ${rootPkg.homepage}
* @license ${rootPkg.license}
*/`
//======================库的配置区域 end==================

export default defineConfig(({ command, mode }): any => {
  // const env = loadEnv(mode, process.cwd(), "");
  const config = {
    plugins: [
      vue({
        reactivityTransform: true //可以在defineProps时解构不丢失响应性
      }),
      vueJsx(),
      VueSetupExtend(), //可以在setup位置定义组件的name
      Components({
        resolvers: [
          ElementPlusResolver({
            importStyle: 'sass'
          })
        ]
      }),
      ElementPlusPlugin({
        useSource: true
      }),
      dts({ insertTypesEntry: true }),
      generatePackageJson(cloneDeep({...rootPkg, outDir}))
    ],

    base: baseUrl + '/',

    define: {
      'import.meta.env.VUE_APP_PROJECT_NAME': JSON.stringify(rootPkg.name)
    },

    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "../src/assets/css/element/element-override.scss" as *;`
        }
      }
    },

    resolve: {
      alias: {
        '@': fileURLToPath(new URL('../src', import.meta.url)),
        _pages: fileURLToPath(new URL('../src/pages', import.meta.url))
      },
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
    },

    envPrefix: 'VUE_',

    // 下面的配置可以进行 将项目打包成一个 库
    build: {
      outDir: outDir,
      lib: {
        entry: fileURLToPath(new URL('../src/main.ts', import.meta.url)),
        formats: ['es', 'cjs', 'umd', 'iife'], //默认['es', 'umd'],所有 ['es' | 'cjs' | 'umd' | 'iife']
        name: Case.pascal(rootPkg.name), //暴露出去的全局变量，使用window['MyLib']调用。在formats为umd和iife时必须。
        fileName: 'dist/' + rootPkg.name //最终生成的js文件的名称。默认为package.json中的项目名
      },
      rollupOptions: {
        // 确保外部化处理那些你不想打包进库的依赖。排除后不会打入lib，但是自己的库被安装的项目里面要安装
        external: ['vue'],
        output: {
          // 在 UMD和iife 构建模式下为这些外部化的依赖提供一个全局变量，配合上面external使用
          globals: {
            vue: 'Vue' //vue是引入的库名, Vue是全局变量名
          },
          banner
        }
      }
    },

    optimizeDeps: {
      // include:[]
    }
  }

  return config
})
