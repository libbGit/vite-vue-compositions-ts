import fs from 'node:fs'

function getPkgJsonStr(rootpkg: any): string {

  delete rootpkg.scripts;
  delete rootpkg['lint-staged'];

  const template = `{
    "name": "${rootpkg.name}",
    "author": "${rootpkg.author}",
    "version": "${rootpkg.version}",
    "description":"${rootpkg.description}",
    "homepage":"${rootpkg.homepage}",
    "license": "${rootpkg.license}",
    "files": ${JSON.stringify(rootpkg.files)},
    "keywords": ${JSON.stringify(rootpkg.keywords)},
    "dependencies": ${JSON.stringify(rootpkg.dependencies)},
    "devDependencies": ${JSON.stringify(rootpkg.devDependencies)},
    "peerDependencies":${JSON.stringify(rootpkg.peerDependencies)},
    "browserslist":${JSON.stringify(rootpkg.browserslist)},
    "engines":${JSON.stringify(rootpkg.engines)},

    "main": "dist/${rootpkg.name}.umd.js",
    "module": "dist/${rootpkg.name}.mjs",
    "types": "dist/${rootpkg.name}.d.ts",
    "exports": {
      ".": {
        "types": "./dist/${rootpkg.name}.d.ts",
        "import": "./dist/${rootpkg.name}.mjs",
        "require": "./dist/${rootpkg.name}.js"
      },
      "./dist/*": "./dist/*"
    }
  }`
  return template;
}

function generatePackageJson(rootpkg: any) {
  return {
    name: 'generate-package-json',
    //当 bundle.write() 结束时调用，此时所有文件都已写入磁盘。所以可以确保dist目录生成，其他都执行完毕，咱们再去生成package.json
    writeBundle() {
      let pkgJson = getPkgJsonStr(rootpkg)
      //写入到 dist目录下的package.json中
      fs.writeFileSync(`./${rootpkg.outDir}/package.json`, pkgJson)

      //把build文件夹下的README.md 复制到 打包后的dist下
      let mdStr = fs.readFileSync('./build/README.md', 'utf-8')
      fs.writeFileSync(`./${rootpkg.outDir}/README.md`, mdStr)
    }
  }
}

export default generatePackageJson
