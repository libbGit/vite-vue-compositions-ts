import { fileURLToPath, URL } from "node:url";

import { defineConfig, loadEnv, splitVendorChunkPlugin } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import dts from 'vite-plugin-dts' //自动生成.dts文件
import Case from 'case'
import VueSetupExtend from "vite-plugin-vue-setup-extend";
import gzipPlugin from "rollup-plugin-gzip";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import ElementPlusPlugin from "unplugin-element-plus/vite";
import rollupPluginClearUnusefile from "rollup-plugin-clear-unusefile";

import pkg from './package.json'

export default defineConfig(({ command, mode }): any => {
  const env = loadEnv(mode, process.cwd(), ""); //mode通过 "vite build --mode test",获得
  const prefixRegx = new RegExp(`^${env.VUE_APP_API_PREFIX}`);

  const config = {
    plugins: [
      vue({
        reactivityTransform: true, //可以在defineProps时解构不丢失响应性
      }),
      vueJsx(),
      VueSetupExtend(), //可以在setup位置定义组件的name
      Components({
        resolvers: [
          ElementPlusResolver({
            importStyle: "sass",
          }),
        ],
      }),
      ElementPlusPlugin({
        useSource: true,
      }),
      dts({insertTypesEntry:true,})
    ],

    base: "/" + pkg.name + "/",

    define: {
      "import.meta.env.VUE_APP_PROJECT_NAME": JSON.stringify(pkg.name),
    },

    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "./src/assets/css/element/element-override.scss" as *;`,
        },
      },
    },

    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
        _pages: fileURLToPath(new URL("./src/pages", import.meta.url)),
      },
      extensions: [".mjs", ".js", ".ts", ".jsx", ".tsx", ".json", ".vue"],
    },

    server: {
      host: "0.0.0.0", //在本地还是 localhost，在别的电脑上用ip，这两联调时，别人可以直接访问自己电脑页面查看
      port: 3000,
      strictPort: true,
      https: false,
      open: false,
      proxy: {
        [env.VUE_APP_API_PREFIX]: {
          target: env.VUE_APP_API_HOST,
          ws: true,
          changeOrigin: true,
          rewrite: (path:string) => path.replace(prefixRegx, ""),
        },
      },
    },

    envPrefix: "VUE_",

    build: {
      target: "modules",
      outDir: "dist/" + pkg.name + "/",
      assetsDir: "assets",
      assetsInlineLimit: 4096,
      cssCodeSplit: true,
      minify: "esbuild",

      rollupOptions: {
        input: {
          index: fileURLToPath(new URL("./index.html", import.meta.url)),
        },
        output: {
          chunkFileNames: "static/js/[name]-[hash].js",
          entryFileNames: "static/js/[name].js",
          assetFileNames: "static/[ext]/[name]-[hash].[ext]",
        },
        plugins: [
          splitVendorChunkPlugin(), // 拆分 chunk 为 index 和 vendor
          gzipPlugin({
            filter: /\.(js|mjs|cjs|json|css|wasm)$/i,
            gzipOptions: {
              level: 9, //值越大压缩越小
            },
            minSize: 10240,
            fileName: ".gz",
          }),
          rollupPluginClearUnusefile({}),
        ],

        // external: ["vue"], // 确保外部化处理那些你不想打包进库的依赖
        // output: {
        //   globals: {
        //     vue: "Vue",
        //   },
        // },
      },
      chunkSizeWarningLimit: 500,
    },

    // // 下面的配置可以进行 将项目打包成一个 库
    // build: {
    //   lib: {
    //     entry: resolve(__dirname, "lib/main.ts"),
    //     name: "MyLib",
    //     formats: ["es", "umd", "iife", "cjs"],
    //     fileName: (format) => `my-lib.${format}.js`,
    //   },
    //   rollupOptions: {
    //     external: ["vue"], // 确保外部化处理那些你不想打包进库的依赖
    //     output: {
    //       // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
    //       globals: {
    //         vue: "Vue",
    //       },
    //     },
    //   },
    // },

    optimizeDeps: {
      // include:[]
    },
  };

  return config;
});
