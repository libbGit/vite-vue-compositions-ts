
## 项目创建

```sh
npm install
```

### 启动本地开发

```sh
npm run dev
```

### 应用的打包

```sh
npm run build
```

### 库的打包

```sh
npm run build:lib
```

### 运行单元测试 [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Playwright](https://playwright.dev)

```sh
# Install browsers for the first run
npx playwright install

# When testing on CI, must build the project first
npm run build

# Runs the end-to-end tests
npm run test:e2e
# Runs the tests only on Chromium
npm run test:e2e -- --project=chromium
# Runs the tests of a specific file
npm run test:e2e -- tests/example.spec.ts
# Runs the tests in debug mode
npm run test:e2e -- --debug
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### commit规范
```sh
pnpm install -g commitizen 
```

commitizen  
- Commitizen是一个基于命令行的交互式工具，它可以帮助开发者规范化提交Git提交信息。 具有 cz命令， git cz
cz-customizable
- 用来定值commitizen中的自动提交信息，定制信息放在 .cz-config.js下
commitlint-config-cz
- 对提交的信息进行校验，是否合理，并配置在 commitlint.config.js 中


"husky"是一个为了方便使用Git hooks的工具，它能够帮助你在项目中自动化地执行一些Git相关的操作。使用husky，你可以在Git的一些关键操作（例如提
交、推送、合并等）前或后，执行一些脚本或命令，比如代码格式化、自动化测试、打包发布等。
- 他可以帮助我们额外拦截一些如git commit等指令。
- 有这些hook  pre-commit， pre-push， post-merge， post-checkout。注意只在Git仓库中才能正常工作
- 在运行 husky install后，会生成.husky目录，在目录下，可以添加hook文件，如 pre-commit


使用lint-staged, 对暂存区代码进行eslint校验和prettier格式化


cz执行流程
```
1、手工git cz   //commitizen暴露出来的命令，
2、加载 .cz-config.js 文件内容
3、手工填写信息
4、通过commitlint-config-cz加载.commitlint.config.js校验输入信息是否合法

如果合法，会自动执行 git commit，接着下面继续

1、手工执行git commit
2、会检测到husky的hook pre-commit，然后执行pre-commit脚本。如果执行不通过，则提交失败，如果执行通过，则成功
3、执行pre-commit中的 npm run lint, 
4、通过package.json中配置的  "lint": "lint-staged"执行package.json中的 同名块 'lint-staged':{xxx}，然后只对对暂存区文件执行格式化
5、格式化完成，完成提交


```

## 项目架构

此模板用户快速的创建项目，采用vite+vue3+ typescript语法，提供两种模板语法，

1、setup方式

2、defineComponent方式

## 库模式时的引入

```sh
yarn add xxx
```

在项目中
```js
import xx from 'xxx'
import xxx/dist/style.css  //引入css
```
