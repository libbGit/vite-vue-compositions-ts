import type { RouteLocationNormalized, RouteRecordRaw } from 'vue-router'
const routes: RouteRecordRaw[] = [
  {
    path: '',
    redirect: '/home'
  },
  {
    path: '/home',

    component: () => import('@/layouts/Index.vue'),
    children: [
      {
        path: '',
        redirect: (to: RouteLocationNormalized) => {
          return {
            path: to.path + '/index'
          }
        }
      },
      {
        path: 'index',
        meta: {
          title: '劳务系统'
        },
        component: () => import(`_pages/home/Index.vue`)
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/Notfound'
  }
]

export default routes
