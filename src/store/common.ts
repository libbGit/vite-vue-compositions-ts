import { defineStore } from 'pinia'
import { stringify, parse } from 'zipson'

type IState = {
  auth: string
  userInfo: any
}

const moduleId = 'user' //模块的id，每个模块之间要唯一
export const useUserStore = defineStore(moduleId, {
  state: (): IState => {
    return {
      auth: '',
      userInfo: {}
    }
  },
  getters: {},

  actions: {
    UPDATE_AUTH(value: any) {
      this.auth = value
    },

    UPDATE_USERINFO(data: any) {
      this.userInfo = { ...this.userInfo, ...data }
    }
  },

  //持久化
  persist: {
    key: `${import.meta.env.VUE_APP_PROJECT_NAME}/${moduleId}`,
    storage: localStorage, //
    paths: ['auth', 'userInfo'], //指定需要持久化的state，不指定，不会存储
    serializer: {
      deserialize: parse,
      serialize: stringify
    },
    debug: false
  }
})
