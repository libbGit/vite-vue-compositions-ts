/// <reference types="vite/client" />

//扩展 Number类型
interface Number {
  plus(num: number): number
  minus(num: number): number
  multiply(num: number): number
  divide(num: number): number
}

//由上面vite/client来定义
// interface ImportMeta {
//   readonly env: ImportMeta;
// }
